﻿using AttendanceWithDatabase.model;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;

using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AttendanceWithDatabase.libraries
{
    class DbOperations
    {
   
        public static DataTable getAllAttendances(DataTable table)
        {

            DbCommand comm = DatabaseConnection.getConnection();

            comm.CommandText = "Select * from attendance ";
            try
            {
                comm.Connection.Open();
                DbDataReader reader = comm.ExecuteReader();
                table.Load(reader);
                reader.Close();
            }
            catch (Exception ex)
            {
                //throw;
            }
            finally
            {
                comm.Connection.Close();
            }
            return table;
        }

        public static void addnewAttendance(string Attandancedate, string teacher_id, string course_id, string room_id, string comment,string startTime,string leaveTime)
        {
            DbCommand comm = DatabaseConnection.getConnection();
            comm.CommandText = "INSERT INTO [attendance] ([date], [teacher_id], [course_id], [room_id], [comment], [startTime], [leaveTime]) VALUES (@Attandancedate, @Teacher, @Course, @Room, @Comment, @Starttime, @Leavetime)";

            DbParameter param = comm.CreateParameter();
            param.ParameterName = "@Attandancedate";
            param.Value = Attandancedate;
            param.DbType = DbType.DateTime;
            param.Size = 20;
            comm.Parameters.Add(param);

            param = comm.CreateParameter();
            param.ParameterName = "@Teacher";
            param.Value = teacher_id;
            param.DbType = DbType.String;
            param.Size = 100;
            comm.Parameters.Add(param);

            param = comm.CreateParameter();
            param.ParameterName = "@Course";
            param.Value = course_id;
            param.DbType = DbType.String;
            param.Size = 100;
            comm.Parameters.Add(param);


            param = comm.CreateParameter();
            param.ParameterName = "@Room";
            param.Value = room_id;
            param.DbType = DbType.String;
            param.Size = 100;
            comm.Parameters.Add(param);


            param = comm.CreateParameter();
            param.ParameterName = "@Comment";
            param.Value = comment;
            param.DbType = DbType.String;
            param.Size = 100;
            comm.Parameters.Add(param);


            param = comm.CreateParameter();
            param.ParameterName = "@Starttime";
            param.Value = startTime;
            param.DbType = DbType.DateTime;
            comm.Parameters.Add(param);


            param = comm.CreateParameter();
            param.ParameterName = "@LeaveTime";
            param.Value = leaveTime;
            param.DbType = DbType.DateTime;
            comm.Parameters.Add(param);




       

            try
            {
                comm.Connection.Open();
              comm.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                
                throw;
            }
            finally
            {
                comm.Connection.Close();
            }

        }


        public static void createAttendance(string attendanceDate, string teacher, string course, string room, string comment,string startTime, string leaveTime)
        {
            DbCommand comm = DatabaseConnection.getConnection();
            String Sql = "INSERT INTO attendance(date, teacher_id, room_id, startTime, leaveTime, course_id, comment) ";
           Sql+= String.Format(" VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}')", attendanceDate, teacher, room, startTime,leaveTime, course, comment);

            comm.CommandText = Sql;


            try
            {
                comm.Connection.Open();
               comm.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
            
                throw;
            }
            finally
            {
                // Close the connection
                comm.Connection.Close();
            }

        }

 
        public static void AddTeacher(String teacherName)
        {
            string sql = "INSERT INTO teachers (TeacherName) VALUES ('"+teacherName+"')";

            DbCommand command = DatabaseConnection.getConnection();
            command.CommandText = sql;

            try
            {
                command.Connection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                //throw;
            }
            finally
            {
                command.Connection.Close();
            }
        }

        public static void AddCourse(String courseName)
        {
            string sql = "INSERT INTO courses (courseName) VALUES ('" + courseName + "')";

            DbCommand command = DatabaseConnection.getConnection();
            command.CommandText = sql;

            try
            {
                command.Connection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                //throw;
            }
            finally
            {
                command.Connection.Close();
            }
        }
        public static void AddRoom(String RoomName)
        {
            string sql = "INSERT INTO rooms (room) VALUES ('" + RoomName + "')";

            DbCommand command = DatabaseConnection.getConnection();
            command.CommandText = sql;

            try
            {
                command.Connection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                //throw;
            }
            finally
            {
                command.Connection.Close();
            }
        }
        public static DataTable getAllTeachers(DataTable table)
        {

            DbCommand comm = DatabaseConnection.getConnection();
    
            comm.CommandText = "Select * from teachers ";
            try
            {
                comm.Connection.Open();
                DbDataReader reader = comm.ExecuteReader();
                table.Load(reader);
                reader.Close();
            }
            catch (Exception ex)
            {
                //throw;
            }
            finally
            {
                comm.Connection.Close();
            }
            return table;
        }

        public static DataTable getAllTCourses(DataTable table)
        {

            DbCommand comm = DatabaseConnection.getConnection();

            comm.CommandText = "Select * from courses ";
            try
            {
                comm.Connection.Open();
                DbDataReader reader = comm.ExecuteReader();
                table.Load(reader);
                reader.Close();
            }
            catch (Exception ex)
            {
                //throw;
            }
            finally
            {
                comm.Connection.Close();
            }
            return table;
        }
        public static DataTable getAllRooms(DataTable table)
        {

            DbCommand comm = DatabaseConnection.getConnection();

            comm.CommandText = "Select * from rooms ";
            try
            {
                comm.Connection.Open();
                DbDataReader reader = comm.ExecuteReader();
                table.Load(reader);
                reader.Close();
            }
            catch (Exception ex)
            {
                //throw;
            }
            finally
            {
                comm.Connection.Close();
            }
            return table;
        }

    }



}
