﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AttendanceWithDatabase.libraries;

namespace AttendanceWithDatabase
{
    public partial class Room : Form
    {
        public Room(Form1 form1)
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DbOperations.AddRoom(textBox1.Text.Trim());
            dataGridView1.DataSource = DbOperations.getAllRooms(new DataTable());
        }

        private void Room_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = DbOperations.getAllRooms(new DataTable());

        }
    }
}
