﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AttendanceWithDatabase.libraries;


namespace AttendanceWithDatabase
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public void Display()
        { 
            
}
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            teacher form = new teacher(this);
            form.ShowDialog();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = DbOperations.getAllAttendances(new DataTable());

            teachers();
            courses();
            rooms();

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void teachers()
        {
            /////////////////////////////////////////////fill teachers list //////////////////////////////////////////

            DataTable table0 = new DataTable();
            table0.Columns.Add("teacherID", typeof(string));
            table0.Columns.Add("Teachername", typeof(string));
            comboBox1.ValueMember = "teacherID";
            comboBox1.DisplayMember = "Teachername";
            comboBox1.DataSource = DbOperations.getAllTeachers(table0);
        }
        private void courses() {
            /////////////////////////////////////////////fill courses list //////////////////////////////////////////
            DataTable table2 = new DataTable();
            table2.Columns.Add("id", typeof(string));
            table2.Columns.Add("Coursename", typeof(string));
            courseCmb.ValueMember = "id";
            courseCmb.DisplayMember = "Coursename";
            courseCmb.DataSource = DbOperations.getAllTCourses(table2);
        
        }

        private void rooms()
        {
            /////////////////////////////////////////////fill rooms list //////////////////////////////////////////

            DataTable table3 = new DataTable();
            table3.Columns.Add("id", typeof(string));
            table3.Columns.Add("room", typeof(string));
            roomCmb.ValueMember = "id";
            roomCmb.DisplayMember = "room";
            roomCmb.DataSource = DbOperations.getAllRooms(table3);
        }
        private void Form1_Shown(object sender, EventArgs e)
        {
           // dataGridView1.DataSource = DbOperations.getAllAttendances();
            

      //   DatabaseAtten.DisplayAttendance("SELECT * FROM attendance", dataGridView1);
          

        }

     

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
           // MessageBox.Show(dateTimePicker1.Value.ToString("yyyy-MM-dd"));
            DbOperations.createAttendance(
                                          attendanceDate: dateTimePicker1.Value.ToString("yyyy-MM-dd"),
                                          teacher: comboBox1.SelectedValue.ToString(),
                                          course: courseCmb.SelectedValue.ToString(),
                                          room: roomCmb.SelectedValue.ToString(),
                                          comment: textBox3.Text,
                                          startTime: dateTimePicker2.Value.TimeOfDay.ToString(),
                                          leaveTime: dateTimePicker3.Value.TimeOfDay.ToString());
            dataGridView1.DataSource = DbOperations.getAllAttendances(new DataTable());

        }

        private void button2_Click(object sender, EventArgs e)
        {
            teacher form = new teacher(this);
            form.ShowDialog();
            teachers();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            course form = new course(this);
            form.ShowDialog();
            courses();
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            Room form = new Room(this);
            form.ShowDialog();
            rooms();
        }
    }
}
