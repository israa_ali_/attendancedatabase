﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AttendanceWithDatabase.libraries;

namespace AttendanceWithDatabase
{
    public partial class course : Form
    {
        public course(Form1 form1)
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DbOperations.AddCourse(textBox1.Text.Trim());
            dataGridView1.DataSource = DbOperations.getAllTCourses(new DataTable());
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void course_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = DbOperations.getAllTCourses(new DataTable());

        }
    }
}
