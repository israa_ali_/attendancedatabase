﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace AttendanceWithDatabase.model
{
    class Attendance
    {
        
        [DisplayName("Date")]
        public String Attandancedate { get; set; }
        [DisplayName("Teacher Name")]
        public Teacher Teacher { get; set; }
        [DisplayName("Course Name")]
        public Course Course { get; set; }
        [DisplayName("Room/Lab Name")]
        public Room Room { get; set; }
        [DisplayName("Start Time")]
        public String Starttime { get; set; }
        [DisplayName("Leave Time")]
        public String Leavetime { get; set; }
        [DisplayName("Note")]
        public String Comment { get; set; }

        public Attendance()
        {
            this.Attandancedate = "";
            this.Teacher = null;
            this.Course = null;
            this.Room = null;
            this.Starttime = "";
            this.Leavetime = "";
            this.Comment = "";
        }
        public Attendance(String attandancedate, Teacher teacher, Course course, Room room, string starttime,
                string leavetime, string comment)
        {

            this.Attandancedate = attandancedate;
            this.Teacher = teacher;
            this.Course = course;
            this.Room = room;
            this.Starttime = starttime;
            this.Leavetime = leavetime;
            this.Comment = comment;

        }


    }
}
